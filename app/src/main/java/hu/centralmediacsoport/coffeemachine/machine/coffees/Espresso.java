package hu.centralmediacsoport.coffeemachine.machine.coffees;

import hu.centralmediacsoport.coffeemachine.machine.ingredients.GroundCoffee;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.Water;

/**
 * Created by david.eperjes on 08/11/16.
 */

public class Espresso extends Coffee /* implements AcceptsExtras */ /* TODO: implement AcceptsExtras interface! */ {

    public static final int COST = 150;

    public Espresso() {
        ingredients.add(new Water());
        ingredients.add(new GroundCoffee());
    }

    @Override
    public String getName() {
        return "Espresso";
    }

    @Override
    public int getCost() {
        return COST;
    }

}
