package hu.centralmediacsoport.coffeemachine.messages;

/**
 * Created by david.eperjes on 08/11/16.
 */

public class CoffeeMachineBalanceMessage {

    private String message;

    public CoffeeMachineBalanceMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
