package hu.centralmediacsoport.coffeemachine.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by david.eperjes on 09/11/16.
 */

public class ConsoleLogMessage {

    private Date mDate;
    private String mMessage;

    private static final DateFormat DF = new SimpleDateFormat("HH:mm:ss");

    public ConsoleLogMessage(String message) {
        this.mMessage = message;
        this.mDate = new Date();
    }

    public String getMessage() {
        return mMessage;
    }

    public String getDateString() {
        return DF.format(mDate);
    }
}
