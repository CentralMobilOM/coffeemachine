package hu.centralmediacsoport.coffeemachine;

import android.content.Context;

public class Application extends android.app.Application {

    private static Context sContext;

    {
        sContext = this;
    }

    public static Context getContext() {
        return sContext;
    }

}